'''
Created on 23 nov. 2020

@author: Denise
'''
from ssl import DER_cert_to_PEM_cert
class Ordenador:
    def ordenarInsercion(self,numeros):
        aux = 0
        
        for i in range(1,len(numeros)):
            aux = numeros[i]
            
            j = (i-1)
            while(j>=0 and numeros[j]>aux):
                numeros[j+1]=numeros[j]
                numeros[j]=aux
                j-=1
     
    def ordenarSeleccion(self,numeros):
        for i in range(0,len(numeros)):
            for j in range(i,len(numeros)):
                if(numeros[i]>numeros[j]):
                    minimo=numeros[i]
                    numeros[i]=numeros[j]
                    numeros[j]=minimo
     
    def quicksort(self, numeros, izq, der):
        pivote = numeros[izq]
        i = izq
        j = der 
        aux = 0
        while i < j:
            while numeros[i] <= pivote and i < j:
                i += 1
            while numeros[j] > pivote:
                j -= 1
            if i < j:
                aux = numeros[i]
                numeros[i]=numeros[j]
                numeros[j]=aux
        numeros[izq]=numeros[j]
        numeros[j]=pivote
        if izq<j-1:
            self.ordenarQuickSort(numeros, izq, j-1)
        self.contador[2]=self.contador[2]+1
        if j+1<der:
            self.ordenarQuickSort(numeros, j+1, der)

                
    def ordenShellsort(self, numeros):
        intervalo = len(numeros)/2
        intervalo = int(intervalo)
        while(intervalo > 0):
            for i in range(int(intervalo), len(numeros)):
                j = i - int(intervalo)
                while(j >= 0):
                    k = j + int(intervalo)
                    if(numeros[j] <= numeros[k]):
                        j =- 1
                    else:
                        aux = numeros[j]
                        numeros[j] = numeros[k]
                        numeros[k] = aux
                        j -= int(intervalo)
            
            intervalo = int(intervalo) / 2
    
    def ordenar(self,numeros,exp1):
        n=len(numeros)
        cantidadDatos=[0]*(n)
        contar=[0]*(10)
        
        for i in range(0,n):
            indice=(numeros[i]/exp1)
            contar[int((indice)%10)]+=1
        for i in range(1,10):
            contar[i]+=contar[i-1]
        i=n-1
        while i>=0:
            indice=(numeros[i]/exp1)
            cantidadDatos[contar[int((indice)%10)]-1]=numeros[i]
            contar[int((indice)%10)]-=1
            i-=1
        i=0
        
        for i in range(0,len(numeros)):
            numeros[i]=cantidadDatos[i]
    
    def radixSort(self,numeros):
        max1=max(numeros)
        exp=1
        while max1/exp>0:
            self.ordenar(numeros,exp)
            exp*=10
            
            
            

op1 = ""
o = Ordenador()
while(op1 != 6):
    print("1- Metodo ordenamiento insercion")
    print("2- Metodo ordenamiento Seleccion")
    print("3- Metodo ordenamiento quicksort")
    print("4- Metodo ordenamiento shellsort")
    print("5- Metodo ordenamiento radix")
    print("6- Salir")
    op1 = input("Elija una opcion")
    
    if(op1 == "1"):
        numeros = [7,2,68,5,1,12,10,70]
        print("Desdordenados: " + str(numeros))
        o.ordenarInsercion(numeros)
        print("Ordenados: " + str(numeros))
    
    elif(op1 == "2"):
        numerosS = [7,2,68,5,1,12,10,70]
        print(f"Desordenados: {numerosS}")
        o.ordenarSeleccion(numerosS)
        print(f"Ordenados: {numerosS}")
    
    elif(op1 == "3"):
        numerosQ = [7,2,68,5,1,12,10,70]
        print(f"Desordenados: {numerosQ}")
        o.quicksort(numerosQ, 0, len(numerosQ) - 1)
        print(f"Ordenados: {numerosQ}")
    
    elif(op1 == "4"):
        numerosSh = [7,2,68,5,1,12,10,70]
        print(f"Desordenados: {numerosSh}")
        o.ordenShellsort(numerosSh)
        print(f"Ordenados: {numerosSh}")
    
    elif(op1 == "5"):
        numerosR = [7,2,68,5,1,12,10,70]
        print(f"Desordenados: {numerosR}")
        o.radixSort(numerosR)
        print(f"Ordenados: {numerosR}")
        
    elif(op1 == "6"):
        print("Saliendo")
        
    else:     
        print("Opcion no valida")



        
    



